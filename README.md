
# aid-chain-composer-model

The project Aid Chain is conceived and developed in response to IBM Natural Disaster Call for Code Challenge 2018 by the Ukrainian team 

  <img width="400" height="400" src="https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-composer-model/uploads/9c1a84bbc460479d06f1f9496e54ed75/ItunesArtwork_2x.png">

# Objective

Build a solution prototype addressing the need for efficient delivery of medical, communication, food, and other supplies in the disaster areas with ruined ground infrastructure. 
The delivery is scheduled on request from people impacted by the disaster and is controlled by charity funds and public. 
The supplies are funded by fiat and crypto currency donations.

# Value

The project allows to deploy geographically distributed, highly available, tampering free, fully open for public inspection aid delivery network based on the enterprise class open source Blockchain platform Hyperledger Fabric. 
The platform records and keeps end-to-end financial and logistic history of donations, spend, and actual aid delivery. 
The taken drone delivery approach (which can be extended with other types of UAV or human driven vehicles) allows for fast and efficient fully traceable aid delivery directly into the hands of an aid seeker in the severe conditions of ruined ground infrastructure. 
Furthermore, the project will enable transparent and quick donating to the cause via public crypto currencies in addition to the conventional currencies while automating crypto currency exchange.

# High-level architecture

Implementation of key part of system – blockchain core is made on Hyperledger Fabric deployed at IBM Cloud;

Drones equipped with GPS module and wi-fi/4G connectivity are used to deliver aid buckets;

The system is integrated with an external crypto-currency exchange system to convert donations to fiat currency;

Global donations are under control of the charity funds committee and voting/approvals are recorded to the DLT;

Inventory and finance operations are automated with smart contracts;

Near real time reports and in-depth/ad hoc analytics are based on Watson IoT connected to drones, Watson Studio, and Cloudant;

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-composer-model/uploads/2682d7bef0367c40573087e107634a43/Screen_Shot_2018-09-18_at_5.15.09_PM.png)

# Model definition

Section contains detailed information about developed model, involved participants, assets, transactions and events.

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-composer-model/uploads/639a9b03cd0d57cd48c7f7efbbebe63e/Screen_Shot_2018-09-18_at_5.17.33_PM.png)

## Participants

1. Aid seeker - a person who creates aid request into the system. 
2. Donor - a person who donates money (via fiat or cryptocurrencies) into particular fund
3. Fund - a charity organization which holds donates
4. Invetory - a place where all purchased items are stored to be delivered
5. Drone - a drone which actually delivers goodies to particular place
6. Exchange processor - internal component which provides exchange capabilities

## Assets

1. Aid Commodity (contains type, description)
2. Aid Bucket (contains status, aid request and set of commodities)
3. Aid Request (contains description, status, list of donors, seeker, required/current Balance and buckets)

## Transactions

1. Open donate transaction (submitted by aid seeker)
2. Make donate transaction (submitted by donor)
3. Purchase bucket transaction (submitted by exchange processor)
4. Inventory bucket registration transaction (submitted by inventory)
5. Start bucket delivery transaction (submitted by drone)
6. Finish bucket delivery transaction (submitted by drone)

## Broadcasted events

1. Aid bucket status changed event
2. Aid request status changed event


# Aidchain team:

Andriy Shapochka ashapoch@softserveinc.com

Dmytro Ovcharenko dovchar@softserveinc.com

Denys Doronin ddor@softserveinc.com