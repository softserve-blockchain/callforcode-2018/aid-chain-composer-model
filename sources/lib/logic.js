/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */

/**
 * Sample transaction processor function.
 * @param {org.aid.processing.OpenDonateTransaction} tx The sample transaction instance.
 * @transaction
 */
async function OpenDonateTransaction(tx) {
  
    tx.asset.requestStatus = "PENDING"
    // Get the asset registry for the asset.
    const assetRegistry = await getAssetRegistry('org.aid.processing.AidRequest');
    // Update the asset in the asset registry.
    // if (!tx.asset.aidBuckets || !tx.asset.aidBuckets.length) {
    //   throw new Error('Request does not contain buckets with commodities');
    // }
    // let requiredBalance = 0.0
    
    // for (var i = 0; i < tx.asset.aidBuckets.length; i++) {
    //   let bucket = tx.asset.aidBuckets[i] 
    //   if ( !bucket.commodities.length) {
    //       throw new Error('Bucket should not be empty. Please check request');
    //     }
    //   for (var j = 0; j < bucket.commodities.length; j++) {
    //     let commodity = bucket.commodities[j]
    //     requiredBalance += commodity.purchasePrice
    //     }
    // }
    // tx.asset.requiredBalance = requiredBalance
    
    await assetRegistry.update(tx.asset);
    
    if (tx.initiator.requests == null) {
      tx.initiator.requests = []
    }
    
    tx.initiator.requests.push(tx.asset)
    
    const seekerRegistry = await getParticipantRegistry('org.aid.processing.AidSeeker');
    // Update the asset in the asset registry.
    let initiator = tx.initiator
    await seekerRegistry.update(initiator);
  
    // Emit an event for the modified asset.
    let event = getFactory().newEvent('org.aid.processing', 'ReqestStatusChanged');
    event.asset = tx.asset;
    event.newStatus = tx.asset.requestStatus;
    emit(event);
  }
  
  /**
   * Sample transaction processor function.
   * @param {org.aid.processing.ApproveRequestTransaction} tx The sample transaction instance.
   * @transaction
   */
  async function ApproveRequestTransaction(tx) {
    
    tx.asset.requestStatus = "APPROVED"
    tx.asset.approver = tx.approver
    if (tx.approver.approvedRequests == null) {
      tx.approver.approvedRequests = []
    }
    
    tx.approver.approvedRequests.push(tx.asset)
    
    const fundRegistry = await getParticipantRegistry('org.aid.processing.Fund');
    // Update the asset in the asset registry.
    await fundRegistry.update(tx.approver);
    
    // Get the asset registry for the asset.
    const assetRegistry = await getAssetRegistry('org.aid.processing.AidRequest');
    // Update the asset in the asset registry.
    
    await assetRegistry.update(tx.asset);
  
    // Emit an event for the modified asset.
    let event = getFactory().newEvent('org.aid.processing', 'ReqestStatusChanged');
    event.asset = tx.asset;
    event.newStatus = tx.asset.requestStatus;
    emit(event);
  }
  /**
   * Sample transaction processor function.
   * @param {org.aid.processing.DonateTransaction} tx The sample transaction instance.
   * @transaction
   */
  async function DonateTransaction(tx) {
    
    //--> Fund destination
    //--> Donor donor
    //o Double amount
    if (tx.amount > tx.donor.balance) {
        throw new Error('Not sufficient funds');
    }
    
    tx.donor.balance = tx.donor.balance - tx.amount
    const donorRegistry = await getParticipantRegistry('org.aid.processing.Donor');
    // Update the asset in the asset registry.
    await donorRegistry.update(tx.donor);
    
    tx.destination.balance = tx.destination.balance + tx.amount
    
    const fundRegistry = await getParticipantRegistry('org.aid.processing.Fund');
    // Update the asset in the asset registry.
    await fundRegistry.update(tx.destination);
  }
  
  /**
   * Sample transaction processor function.
   * @param {org.aid.processing.PurchaseBucketTransaction} tx The sample transaction instance.
   * @transaction
   */
  async function PurchaseBucketTransaction(tx) {
    //--> AidBucket asset
    //--> ExchangeProcessor processor
    //bucketsToProcess
    if(tx.processor.bucketsToProcess == null) {
      tx.processor.bucketsToProcess = [];
    }
    
    
    let requiredBalance = 0.0
    if ( !tx.asset.commodities.length) {
        throw new Error('Bucket should not be empty. Please check');
      }
    for (var j = 0; j < tx.asset.commodities.length; j++) {
      let commodity = tx.asset.commodities[j]
      const res = await getAssetRegistry('org.aid.processing.AidCommodity')
      .then(function (commodityRegistry) {
      // Get the specific vehicle from the vehicle asset registry.
      return commodityRegistry.get(commodity.entityId);
      })
      .then(function (fetchedCommodity) {
      // Process the the vehicle object.
        console.log(fetchedCommodity.entityId);
        requiredBalance += fetchedCommodity.purchasePrice
      })
      
    }

    if (requiredBalance >  tx.asset.aidRequest.approver.balance) {
      throw new Error('Not sufficient funds for approver');
    }

    tx.asset.aidRequest.approver.balance -= requiredBalance

    const approverRegistry = await getParticipantRegistry('org.aid.processing.Fund');
    // Update the asset in the asset registry.
    await approverRegistry.update(tx.asset.aidRequest.approver); 

    // adjust request funds amount? 
    tx.processor.bucketsToProcess.push(tx.asset); 
    
    const aidProcessorRegistry = await getParticipantRegistry('org.aid.processing.ExchangeProcessor');
    // Update the asset in the asset registry.
    await aidProcessorRegistry.update(tx.processor);
    tx.asset.bucketStatus = "PURCHASED"
    const aidBucketRegistry = await getAssetRegistry('org.aid.processing.AidBucket');
    // Update the asset in the asset registry.
    await aidBucketRegistry.update(tx.asset); 
    
    let event = getFactory().newEvent('org.aid.processing', 'AidBucketStatusChanged');
    event.asset = tx.asset;
    event.newStatus = tx.asset.bucketStatus;
    emit(event);
  }
  
  /**
   * Sample transaction processor function.
   * @param {org.aid.processing.InventoryBucketRegistrationTransaction} tx The sample transaction instance.
   * @transaction
   */
  async function InventoryBucketRegistrationTransaction(tx) {
    //--> AidBucket asset
    //--> Inventory targetInventory
    
    if (!tx.asset.aidRequest) {
      throw new Error('Bucket should be assigned to Request before registration.');
    }

    if(tx.targetInventory.buckets == null) {
      tx.targetInventory.buckets = [];
    }
    
    // adjust request funds amount? 
    tx.targetInventory.buckets.push(tx.asset); 
    
    const aidInventoryRegistry = await getParticipantRegistry('org.aid.processing.Inventory');
    // Update the asset in the asset registry.
    await aidInventoryRegistry.update(tx.targetInventory);
    
    tx.asset.bucketStatus = "INVENTORY"
    tx.asset.inventory = tx.targetInventory
    const aidBucketRegistry = await getAssetRegistry('org.aid.processing.AidBucket');
    // Update the asset in the asset registry.
    await aidBucketRegistry.update(tx.asset); 
    
    let event = getFactory().newEvent('org.aid.processing', 'AidBucketStatusChanged');
    event.asset = tx.asset;
    event.newStatus = tx.asset.bucketStatus;
    emit(event);
  }
  
  /**
   * Sample transaction processor function.
   * @param {org.aid.processing.StartBucketDeliveryTransaction} tx The sample transaction instance.
   * @transaction
   */
  async function StartBucketDeliveryTransaction(tx) {
    //--> AidBucket asset
    //--> Drone targetDrone
    if (tx.asset.bucketStatus != "INVENTORY") {
      throw new Error("Bucket status incompatible with tx: " + tx.asset.bucketStatus);
    }
    tx.targetDrone.targetBucket = tx.asset
    
    const aidDroneRegistry = await getParticipantRegistry('org.aid.processing.Drone');
    // Update the asset in the asset registry.
    await aidDroneRegistry.update(tx.targetDrone);
    
    tx.asset.bucketStatus = "INPROGRESS"
    const aidBucketRegistry = await getAssetRegistry('org.aid.processing.AidBucket');
    // Update the asset in the asset registry.
    await aidBucketRegistry.update(tx.asset); 
    
    let event = getFactory().newEvent('org.aid.processing', 'AidBucketStatusChanged');
    event.asset = tx.asset;
    event.newStatus = tx.asset.bucketStatus;
    emit(event);
  }
  
  /**
   * Sample transaction processor function.
   * @param {org.aid.processing.FinishBucketDeliveryTransaction} tx The sample transaction instance.
   * @transaction
   */
  async function FinishBucketDeliveryTransaction(tx) {
    if (tx.asset.bucketStatus != "INPROGRESS") {
      throw new Error("Bucket status incompatible with tx: " + tx.asset.bucketStatus);
    }
    
    tx.targetDrone.targetBucket = null
    
    const aidDroneRegistry = await getParticipantRegistry('org.aid.processing.Drone');
    // Update the asset in the asset registry.
    await aidDroneRegistry.update(tx.targetDrone);
    
    tx.asset.bucketStatus = "DELIVERED"
    const aidBucketRegistry = await getAssetRegistry('org.aid.processing.AidBucket');
    // Update the asset in the asset registry.
    await aidBucketRegistry.update(tx.asset); 
    
    let event = getFactory().newEvent('org.aid.processing', 'AidBucketStatusChanged');
    event.asset = tx.asset;
    event.newStatus = tx.asset.bucketStatus;
    emit(event);
  }
