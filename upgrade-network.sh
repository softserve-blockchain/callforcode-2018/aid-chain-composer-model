#!/usr/bin/env bash

if [[ $# -eq 0 ]] ; then
    echo 'Pass a version number same as in package.json!'
    exit 1
fi

VERSION=$1

composer archive create -t dir -n ./sources -a aid-processing.bna
composer network install -a aid-processing.bna -c adminCard
# composer network upgrade -n aid-processing -V $VERSION  -c admin@aid-processing -o endorsementPolicyFile=./endorsementPolicy.json
